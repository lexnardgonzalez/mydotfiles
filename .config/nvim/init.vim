
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General Settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
filetype plugin indent on  	    " Enabling Plugin & Indent
set nobackup                    " No auto backups
set t_Co=256                    " Set if term supports 256 colors.
set wildmode=longest,list,full  " Enable autocompletion, ctrl+n to activate.

set clipboard=unnamedplus       " Copy/paste between vim and other programs using system clipboard.
vnoremap <C - c> "+y
map <C - p> "+p

set hlsearch			        " Highlights searches.
set incsearch			        " Display matches for a search pattern while you type. 
set number relativenumber       " Display line numbers relatively.
set ruler			            " Always display current cursor position in the lower right.
set showcmd			            " Display an incomplete comman in the lower right corner as we type.
syntax on 			            " Syntax highlighting. 

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Remap Keys
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" This makes so after your search you can hit return again to make the highlithing disappear.
nnoremap <CR> :noh<CR><CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Status Line
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set statusline=
set statusline+=%#IncSearch#
set statusline+=\ %y
set statusline+=\ %r
set statusline+=%#CursorLineNr#
set statusline+=\ %F
set statusline+=%=              "Right side settings
set statusline+=%#Search#
set statusline+=\ %l/%L
set statusline+=\ [%c]

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set expandtab                   " Converts our tabs to spaces. 
set shiftwidth=4                " One tab == four spaces.
set softtabstop=4	        	" One tab == four spaces.
set tabstop=4                   " One tab == four spaces.
